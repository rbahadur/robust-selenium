package io.template.lib;

import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.lib.util.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.events.AddParameterEvent;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Page {

    public static String returnItemFromStashIfExist(final String item) {
        if (item.contains("stash")) {
            String[] items = item.split("_");
            if (null != Init.getStash().get(items[1]))
                return Init.getStash().get(items[1]).toString();
        }
        return item;
    }

    protected void waitForVisibilityOf(By locator) {
        List<WebElement> elements = Init.getDriver().findElements(locator);
        int i = 0;
        boolean isVisible = false;
        while (i < 10
                && elements.size() < 0
                && !isVisible) {
            try {
                Thread.sleep(2 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            elements = Init.getDriver().findElements(locator);
            if (elements.size() > 0 && elements.get(0).isDisplayed()) {
                isVisible = true;
            }
            i++;
        }
    }

    /**
     * <p>
     * getTitle.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitle() {
        return this.getClass().getAnnotation(PageEntry.class).title();
    }

    public void takeAction(String action, Object... param) throws Throwable {
        action = action.replaceAll(" ", "_");
        try {
            MethodUtils.invokeMethod(this, action, param);
        } catch (NoSuchMethodException e) {
            StringBuilder sb = new StringBuilder();

            sb.append("There is no \"").append(action).append("\" action ")
                    .append("in ")
                    .append(this.getTitle()).append(" page object")
                    .append("\n");
            sb.append("Possible actions are:")
                    .append("\n");
            Class tClass = this.getClass();
            Method[] methods = tClass.getDeclaredMethods();
            for (Method method : methods) {
                sb.append("\t\"")
                        .append(this.getTitle()).append("\"->\"")
                        .append(method.getName()).append("\" with ")
                        .append(method.getGenericParameterTypes().length)
                        .append(" input parameters").append("\n");
            }
            throw new NoSuchMethodException(sb.toString());
        } catch (InvocationTargetException ex) {
            throw ex.getCause();
        }
    }

    public WebElement findElementByTitle(String title) throws IllegalAccessException {
        List<Field> fieldList = FieldUtils.getDeclaredFieldsWithInheritanse(this.getClass());

        for (Field field : fieldList) {
            for (Annotation annotation : field.getAnnotations()) {
                if (annotation instanceof ElementTitle
                        && ((ElementTitle) annotation).value().equals(title)) {
                    field.setAccessible(true);
                    return (WebElement) field.get(this);
                }
            }
        }

        for (Field field : fieldList) {
            for (Annotation annotation : field.getAnnotations()) {
                if (annotation instanceof FindBy
                        && (((FindBy) annotation).css().contains(title)
                        || ((FindBy) annotation).linkText().equals(title)
                        || ((FindBy) annotation).name().equals(title)
                        || ((FindBy) annotation).partialLinkText().equals(title)
                        || ((FindBy) annotation).tagName().equals(title)
                        || ((FindBy) annotation).xpath().equals(title))) {
                    field.setAccessible(true);
                    return (WebElement) field.get(this);
                } else if (annotation instanceof ElementTitle
                        && ((ElementTitle) annotation).value().equals(title)) {
                    field.setAccessible(true);
                    return (WebElement) field.get(this);
                }
            }
        }
        throw new NoSuchElementException("there is no " + title + " web element on " + this.getTitle()
                + " page object,");
    }

    public void press_by_link(String element) throws Exception {
        WebElement webElement;
        try {
            webElement = Init.getPageFactory().getCurrentPage().findElementByTitle(element);
        } catch (Exception | AssertionError e) {
            System.err.println("Failed to find element by title " + element + ". Error message = " + e.getMessage());
            webElement = Init.getDriverExtensions().waitUntilElementAppearsInDom(By.partialLinkText(element));
        }
        Init.getDriverExtensions().waitForElementGetEnabled(webElement, Init.getTimeOut());
        press_by_link(webElement);
    }

    public void press_by_link(WebElement element) throws Exception {
        toAllure("Pressed button with title", getElementTitle(element));
        element.click();
    }

    public void press_button(String element) throws Exception {
        press_by_link(element);
    }

    public void press_button(WebElement webElement) throws IllegalAccessException {
        toAllure("Pressed button with title", getElementTitle(webElement));
        webElement.click();
    }

    private String getElementTitle(Object element) throws IllegalAccessException {
        Page currentPage = Init.getPageFactory().currentPage;
        if (null == currentPage) {
            toAllure("Current page not initialized yet.", "You must initialize it by hands at first time only.");
            return null;
        }
        List<Field> fields = FieldUtils.getDeclaredFieldsWithInheritanse(currentPage);
        try {
            Field requiredField = fields.stream().filter(field -> {
                try {
                    return WebElement.class.isAssignableFrom(field.getType())
                            && element.equals(field.get(currentPage))
                            && null != field.getAnnotation(ElementTitle.class);
                } catch (IllegalAccessException e) {
//                    System.err.println("Stream. IllegalAccessException. Error message = " + e.getMessage());
                    return false;
                }
            }).findFirst().orElse(null);
            Assert.assertNotNull(requiredField);
            return requiredField.getAnnotation(ElementTitle.class).value();
        } catch (NoSuchElementException | StaleElementReferenceException | NullPointerException e) {
//            System.err.println("Stream. Failed to get element value. Error message = " + e.getMessage());
        }
        return null;
    }

    public void fill_field(String element, String text) throws Exception {
        WebElement webElement = Init.getPageFactory().getCurrentPage().findElementByTitle(element);
        toAllure("Fill field " + Init.getPageFactory().getCurrentPage().getElementTitle(webElement) +
                ", with text", text);
        try {
            webElement.click();
        } catch (WebDriverException e) {
            toAllure("Error when try to click on element " +
                    Init.getPageFactory().getCurrentPage().getElementTitle(element), e.getMessage());
            System.err.println("Error when try to click on element " +
                    Init.getPageFactory().getCurrentPage().getElementTitle(element) + ". Error - " + e);
        }
        try {
            webElement.clear();
        } catch (InvalidElementStateException e) {
            System.err.println("Failed to clear web element. Error message = " + e.getMessage());
        }
        webElement.sendKeys(text);
    }

    public void fill_field(WebElement element, String text) throws Exception {
        if (null != text) {
            toAllure("Fill field " + Init.getPageFactory().getCurrentPage().getElementTitle(element) +
                    ", with text", text);
            try {
                element.click();
            } catch (WebDriverException e) {
                System.err.println("Error when try to click on element " +
                        Init.getPageFactory().getCurrentPage().getElementTitle(element) + ". Error - " + e);
            }
            try {
                element.clear();
            } catch (InvalidElementStateException e) {
                System.err.println("Failed to clear web element. Error message = " + e.getMessage());
            }
            element.sendKeys(text);
        } else
            System.err.println("For field " + Init.getPageFactory().getCurrentPage().getElementTitle(element) +
                    " text == null.");
    }

    public void sleep(int i) throws InterruptedException {
        Thread.sleep(i * 1000);
    }

    public void scrollTo(WebElement element) throws InterruptedException {
        ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(1000);
    }

    public void press_key(String keyName) {
        Keys key;
        try {
            key = Keys.valueOf(keyName.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Can't click button: " + keyName, e);
        }
        Actions actions = new Actions(Init.getDriver());
        actions.sendKeys(key).perform();
    }

    public void press_button_by_js(WebElement element) throws InterruptedException {
        ((JavascriptExecutor) Init.getDriver()).executeScript("arguments[0].click();", element);
        Thread.sleep(1000);
    }

    protected void toAllure(String name, String value) {
        Allure.LIFECYCLE.fire(new AddParameterEvent(name, value));
    }

}
