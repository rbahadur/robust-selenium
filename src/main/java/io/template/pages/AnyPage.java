package io.template.pages;

import autoitx4java.AutoItX;
import com.jacob.com.LibraryLoader;
import io.template.lib.Init;
import io.template.lib.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.util.List;
import java.util.Set;

/**
 * Created by Nestor on 18.03.2016.
 */
public abstract class AnyPage extends Page {

    public static void setPageOnTop(String title) {
        File file = getLibAutoIT();
        System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());
        try {
            AutoItX autoItX = new AutoItX();
            autoItX.setOption("WinTitleMatchMode", "2");
            autoItX.winActivate(title);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File getLibAutoIT() {
        File file;
        if (System.getProperty("sun.arch.data.model").contains("64"))
            file = new File("lib", "jacob-1.18-x64.dll");
        else
            file = new File("lib", "jacob-1.18-x86.dll");
        return file;
    }

    public static void uploadFile(String path) {
        File file = getLibAutoIT();
        System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());
        try {
            AutoItX autoItX = new AutoItX();
            String[][] lists = autoItX.winList("");
            autoItX.winWait(lists[0][1]);
            autoItX.controlFocus(lists[0][1], "", "Edit1");
            autoItX.controlSend(lists[0][1], "", "Edit1", path.replace(";", ":"));
            autoItX.controlClick(lists[0][1], "", "Button1");
        } catch (Exception e) {
            System.err.println("error = " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void wait_new_handler(String handlerCount) {
        try {
            int repeatCount = 0;
            while (Init.getDriver().getWindowHandles().size() != Integer.parseInt(handlerCount) && repeatCount < 10) {
                sleep(1);
                repeatCount++;
            }
        } catch (Exception e) {
            System.err.println("Error when wait new handler. Message = " + e.getMessage());
        }
    }

    public void switch_to_the_new_page() throws Exception {
        String newHandler = Init.getDriverExtensions().
                findNewWindowHandle((Set<String>) Init.getStash().get("mainWindowHandle"), Init.getTimeOut());
        Init.getDriver().switchTo().window(newHandler);
        Init.getDriver().switchTo().defaultContent();
        Init.getDriver().switchTo().activeElement();
    }

    public void close_new_page() throws Exception {
        Init.getDriver().close();
        Init.getDriver().switchTo().window(((Set<String>) Init.getStash().get("mainWindowHandle")).iterator().next());
        Init.getDriver().switchTo().defaultContent();
        Init.getDriver().switchTo().activeElement();
    }

    protected WebElement findElementByTextFromList(By list, String text) {
        Init.getDriverExtensions().waitForPageToLoad();
        List<WebElement> items = Init.getDriver().findElements(list);
        return items.stream().filter(el -> el.getText().equalsIgnoreCase(text)).findFirst().orElse(null);
    }

    protected WebElement findElementByTextFromList(By list, By inner, String text) {
        Init.getDriverExtensions().waitForPageToLoad();
        List<WebElement> items = Init.getDriver().findElements(list);
        return items.stream().filter(el -> el.findElement(inner).getText().
                equalsIgnoreCase(text)).findFirst().orElse(null);
    }

}
