package io.template.pages;

import io.template.lib.Init;
import io.template.lib.Props;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by sbt-velichko-aa on 26.12.2016.
 */
@PageEntry(title = "LoginPage")
public class LoginPage extends AnyPage{

    @FindBy(id = "username")
    @ElementTitle("username")
    public WebElement inputUsername;

    @FindBy(xpath = ".//*[@id='password']")
    @ElementTitle("password")
    public WebElement inputPassword;

    @FindBy(xpath = ".//*[@id='loginForm']/div[5]/div/button")
    @ElementTitle("login")
    public WebElement buttonLogin;

    @FindBy(linkText = "Forgot Password?")
    @ElementTitle("Forgot Password")
    public WebElement linkForgotPassword;

    @FindBy(linkText = "Request an Account")
    @ElementTitle("Request an Account")
    public WebElement linkRequestAnAccount;

    @FindBy(id = "loginForm")
    @ElementTitle("loginForm")
    public WebElement divLoginForm;

    @FindBy(css = "dashboard-area")
    @ElementTitle("dashboard area")
    public WebElement divDashboardArea;

    @FindBy(id = "error-msg")
    @ElementTitle("error msg")
    public WebElement divErrorMsg;

    public LoginPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(inputUsername));
    }


    public void check_condition() {
        Assert.assertTrue("Alert is present", divErrorMsg.isDisplayed());
    }

    public void login_into_system() throws Exception {
        fill_field(inputUsername, Props.get("user.uid"));
        fill_field(inputPassword, Props.get("user.password"));
        press_button(buttonLogin);
    }
}
