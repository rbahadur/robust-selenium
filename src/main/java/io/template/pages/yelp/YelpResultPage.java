package io.template.pages.yelp;

import io.template.lib.Init;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.pages.AnyPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Yelp. Result")
public class YelpResultPage extends AnyPage {

    @FindBy(css = "h1.biz-page-title")
    @ElementTitle("initElem")
    public WebElement initElem;

    @FindBy(css = "div.map-box-address address")
    @ElementTitle("address")
    public WebElement address;

    @FindBy(css = "span.biz-phone")
    @ElementTitle("phone")
    public WebElement spanPhone;

    @FindBy(css = "div.review-content")
    @ElementTitle("phone")
    public List<WebElement> divReviews;


    public YelpResultPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds())
                .until(ExpectedConditions.visibilityOf(initElem));
    }

    public void log_critical_information() {
        System.out.println("Log name: " + address.getText());
        toAllure("Log name: ", address.getText());
        System.out.println("Log Phone: " + spanPhone.getText());
        toAllure("Log Phone: ", spanPhone.getText());
        for (int i = 0; i < 3; i++) {
            System.out.println("review " + (i + 1) + ": " + divReviews.get(i).getText());
            toAllure("review " + (i + 1) + ": ", divReviews.get(i).getText());
        }
    }
}
