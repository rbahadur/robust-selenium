Feature: initial test

  @yelp
  Scenario Outline: initial test
    * User go to the yelp.com
    * User prepare test data from file "<testData>"
    * User is on page "Yelp. Search"
    * User (select category)
    * User (append to search string)
    * User (report page count)
    * User (set filter)
    * User (report page count)
    * User (report star rating)
    * User (open result) "1"
    * User is on page "Yelp. Result"
    * User (log critical information)

    Examples:
      | testData   |
      | data.xlsx  |
      | data1.xlsx |
      | data2.xlsx |
